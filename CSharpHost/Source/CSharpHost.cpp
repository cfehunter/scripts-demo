#include <array>
#include <string>
#include <memory>
#include <iostream>
#include <Windows.h>

#include "CSharpHost.h"
#include "mscoree.h" //Make sure this is the local version and not the version in the Windows SDK.

namespace CAMPAIGN
{
	class CAMPAIGN_MODEL;
}

using BENCHMARK_FUNC = void(*)(int);

//Static data
ICLRRuntimeHost4*             s_runtime_host = nullptr;
ICLRAppDomainResourceMonitor* s_resource_monitor = nullptr;
DWORD                         s_app_domain_id = 0;
ICLRControl*                  s_clr_control = nullptr;
BENCHMARK_FUNC                 s_benchmark_func = nullptr;
BENCHMARK_FUNC                 s_benchmark_fib_fnc = nullptr;

const std::wstring& current_directory()
{
	static const std::wstring c_current_directory = []()
	{
		DWORD path_length = GetCurrentDirectoryW(0, nullptr);

		std::unique_ptr<wchar_t[]> buffer = std::make_unique<wchar_t[]>(path_length);
		GetCurrentDirectoryW(path_length, buffer.get());
		return std::wstring(buffer.get());
	}();

	return c_current_directory;
}

//CLR expects a semi-colon separated list of binaries to give full platform trust to
//Given that our LUA system is completely un-sandboxed (ask Mitch about his exe in a pack file), I don't think this is a big concern
std::wstring collect_trusted_binaries()
{
	//Collect every dll in the same folder as game.exe

	std::wstring result;
	WIN32_FIND_DATAW file_find_data;

	static const std::wstring dll_ext = L".dll";
	std::wstring current_file_name = current_directory() + L"\\*";

	HANDLE file_handle = FindFirstFileW(current_file_name.data(), &file_find_data);

	if (file_handle == INVALID_HANDLE_VALUE)
	{
		return result;
	}

	do
	{
		if ((file_find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY)
		{
			current_file_name.assign(file_find_data.cFileName);
			for (std::wstring::const_reverse_iterator ext_itr = dll_ext.rbegin(), file_itr = current_file_name.rbegin(); file_itr != current_file_name.rend(); ++ext_itr, ++file_itr)
			{
				if (ext_itr == dll_ext.rend())
				{
					result += current_directory() + L"\\" + current_file_name + L";";
					break;
				}
				else if (*file_itr != *ext_itr)
				{
					break;
				}
			}
		}
	} while (FindNextFileW(file_handle, &file_find_data));

	return result;
}

bool initialise_app_domain(DWORD& domain_id)
{
	std::array<const wchar_t*, 4> domain_property_keys{
	   L"TRUSTED_PLATFORM_ASSEMBLIES",
	   L"APP_PATHS",
	   L"NATIVE_DLL_SEARCH_DIRECTORIES",
	   L"APP_NI_PATHS"
	};

	const std::wstring& working_directory = current_directory();
	const std::wstring trusted_binaries = collect_trusted_binaries();
	const std::wstring app_paths = working_directory + L"\\DotNETCore;" + working_directory + L"\\Scripts";

	std::array<const wchar_t*, 4> domain_property_values{
		trusted_binaries.data(),
		app_paths.data(),
		working_directory.data(),
		working_directory.data()
	};

	const DWORD app_domain_flags =
		APPDOMAIN_ENABLE_PLATFORM_SPECIFIC_APPS |
		APPDOMAIN_ENABLE_PINVOKE_AND_CLASSIC_COMINTEROP |
		APPDOMAIN_DISABLE_TRANSPARENCY_ENFORCEMENT;

	HRESULT result = s_runtime_host->CreateAppDomainWithManager(
		L"GAME SCRIPT",
		app_domain_flags,
		nullptr,
		nullptr,
		static_cast<int>(domain_property_keys.size()),
		domain_property_keys.data(),
		domain_property_values.data(),
		&domain_id
	);

	if (FAILED(result))
	{
		std::cout << "Error: Failed to create app domain. Error Code: " << result << '\n';
		return false;
	}

	return true;
}

void print_mem_stats_impl(const DWORD appdomain, ICLRAppDomainResourceMonitor& resource_monitor)
{
	ULONGLONG total_bytes = 0;

	HRESULT result = resource_monitor.GetCurrentAllocated(appdomain, &total_bytes);
	if (FAILED(result))
	{
		std::cout << "Error: Failed to retreive total allocated memory with code: " << result << '\n';
		return;
	}

	ULONGLONG mb = total_bytes / (1024 * 1024);
	total_bytes -= mb * (1024 * 1024);
	ULONGLONG kb = total_bytes / 1024;
	total_bytes -= kb * 1024;


	std::cout << "Total Allocated Memory (GC May Be Pending): " << mb << " MB, " << kb << " KB, " << total_bytes << " Bytes\n";
}

void handle_appdomain_unhandled_exception(wchar_t* exception_object, wchar_t* exception_message, wchar_t* /*stack_trace*/)
{
	MessageBoxW(nullptr, exception_object, exception_message, MB_OK);
}

extern "C"
{
	const char* name() { return "CSharpHost"; }

	bool bootstrap(void* campaign_model)
	{
		// Load the CoreCLR Library ///////////////
		HMODULE coreclr_lib = LoadLibraryA("DotNETCore/coreclr.dll");
		if (!coreclr_lib)
		{
			const DWORD error_code = GetLastError();
			std::cout << "Error: Failed to load CoreCLR library with error code: " << error_code << '\n';
			return false;
		}

		FnGetCLRRuntimeHost get_clr_runtime_host = reinterpret_cast<FnGetCLRRuntimeHost>(GetProcAddress(coreclr_lib, "GetCLRRuntimeHost"));
		if (get_clr_runtime_host == nullptr)
		{
			const DWORD error_code = GetLastError();
			std::cout << "Error: Failed to get method \"GetCLRRuntimeHost\" method from CoreCLR lib. Error Code: " << error_code << '\n';
			return false;
		}

		// Start the Runtime /////////////////////
		HRESULT hr = get_clr_runtime_host(IID_ICLRRuntimeHost4, reinterpret_cast<IUnknown**>(&s_runtime_host));
		if (FAILED(hr))
		{
			std::cout << "Error: Failed to instantiate runtime host. Error Code: " << hr << '\n';
			return false;
		}

		//Must create control interface before startup
		hr = s_runtime_host->GetCLRControl(&s_clr_control);
		if (FAILED(hr))
		{
			std::cout << "\nError: Failed to get clr control. Error Code: " << hr << '\n';
			return false;
		}

		//We have no need for multiple app domains and the server GC doesn't fit our use case
		hr = s_runtime_host->SetStartupFlags(static_cast<STARTUP_FLAGS>(
			STARTUP_FLAGS::STARTUP_CONCURRENT_GC |
			STARTUP_FLAGS::STARTUP_SINGLE_APPDOMAIN |
			STARTUP_FLAGS::STARTUP_LOADER_OPTIMIZATION_SINGLE_DOMAIN
			));

		if (FAILED(hr))
		{
			std::cout << "Error: Failed to set instance startup flags. Error Code: " << hr << '\n';
			return false;
		}

		hr = s_runtime_host->Start();
		if (FAILED(hr))
		{
			std::cout << "Error: Failed to start .NET runtime host. Error Code: " << hr << '\n';
			return false;
		}

		// Initialise App Domain //////////////////
		if (!initialise_app_domain(s_app_domain_id))
		{
			std::cout << "Error: Failed to initialise app domain. Exiting\n";
			return false;
		}

		// We opt-in to resource tracking by requesting this interface. Best not to in a release build
		hr = s_clr_control->GetCLRManager(IID_ICLRAppDomainResourceMonitor, reinterpret_cast<void**>(&s_resource_monitor));
		if (FAILED(hr))
		{
			std::cout << "\nError: Failed to get app domain resource monitor. Error Code: " << hr << '\n';
			return false;
		}

		//Create a callback into the managed library to bootstrap the script system
		using SCRIPT_BOOTSTRAP_FUNC = void(*)(CAMPAIGN::CAMPAIGN_MODEL& model);
		SCRIPT_BOOTSTRAP_FUNC scripts_bootstrap = nullptr;
		hr = s_runtime_host->CreateDelegate(
			s_app_domain_id,
			L"GameScript.Core",
			L"GameScript.Core.Bootstrap",
			L"bootstrap",
			reinterpret_cast<INT_PTR*>(&scripts_bootstrap)
		);

		if (FAILED(hr))
		{
			std::cout << "Error: Failed to get bootstrap method from scripts library. Error Code: " << hr << '\n';
			return false;
		}

		hr = s_runtime_host->CreateDelegate(
			s_app_domain_id,
			L"GameScript.Core",
			L"GameScript.Core.Bootstrap",
			L"benchmark",
			reinterpret_cast<INT_PTR*>(&s_benchmark_func)
		);

		if (FAILED(hr))
		{
			std::cout << "Error: failed to get benchmark method from scripts library. Error Code: " << hr << '\n';
			return false;
		}
		hr = s_runtime_host->CreateDelegate(
			s_app_domain_id,
			L"GameScript.Core",
			L"GameScript.Core.Bootstrap",
			L"benchmark_fib",
			reinterpret_cast<INT_PTR*>(&s_benchmark_fib_fnc)
		);

		if (FAILED(hr))
		{
			std::cout << "Error: failed to get fibonnaci benchmark method from scripts library. Error Code: " << hr << '\n';
			return false;
		}

		scripts_bootstrap(*reinterpret_cast<CAMPAIGN::CAMPAIGN_MODEL*>(campaign_model));

		return true;
	}

	void run_benchmark(int count)
	{
		s_benchmark_func(count);
	}

	void run_benchmark_fib(int count)
	{
		s_benchmark_fib_fnc(count);
	}

	void unload_lib()
	{
		// Shutdown CLR Host //////////////////////
		if (s_resource_monitor)
		{
			s_resource_monitor->Release();
			s_resource_monitor = nullptr;
		}

		if (s_runtime_host)
		{
			s_runtime_host->UnloadAppDomain(s_app_domain_id, true);
			s_runtime_host = nullptr;
		}
		
		if (s_clr_control)
		{
			s_clr_control->Release();
			s_clr_control = nullptr;
		}

		if (s_runtime_host)
		{
			s_runtime_host->Stop();
			s_runtime_host->Release();
			s_runtime_host = nullptr;
		}

		s_app_domain_id = 0;
	}

	void print_mem_stats()
	{
		ULONGLONG total_bytes = 0;

		HRESULT result = s_resource_monitor->GetCurrentAllocated(s_app_domain_id, &total_bytes);
		if (FAILED(result))
		{
			std::cout << "Error: Failed to retreive total allocated memory with code: " << result << '\n';
			return;
		}

		ULONGLONG mb = total_bytes / (1024 * 1024);
		total_bytes -= mb * (1024 * 1024);
		ULONGLONG kb = total_bytes / 1024;
		total_bytes -= kb * 1024;


		std::cout << "Total Allocated Memory (GC May Be Pending): " << mb << " MB, " << kb << " KB, " << total_bytes << " Bytes\n";
	}
}