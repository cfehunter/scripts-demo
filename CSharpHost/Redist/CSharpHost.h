#pragma once

#include <Windows.h>


extern "C"
{
	__declspec(dllexport) const char* name();
	__declspec(dllexport) bool        bootstrap(void* campaign_model);
	__declspec(dllexport) void        unload_lib();

	//None of this needs to exist, it's just for the existing demo behaviour
	__declspec(dllexport) void        run_benchmark(int count);
	__declspec(dllexport) void        run_benchmark_fib(int count);
	__declspec(dllexport) void        print_mem_stats();
}