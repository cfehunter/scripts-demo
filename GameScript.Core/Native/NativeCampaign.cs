﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace GameScript.Core.Native
{
	//This attribute stops the CLR from creating a stack wall to check if the calling code has permissions to call native libs.
	//In our case, our entire library stack should have access to native libs, so turning this off is a potentially massive performance gain
	[SuppressUnmanagedCodeSecurity]
	internal static class NativeCampaignModel
	{
		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_model_factions")]
		internal static extern IntPtr Factions(IntPtr model, out uint size);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_model_current_turn_faction")]
		internal static extern IntPtr CurrentTurnFaction(IntPtr model);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_model_random_number")]
		internal static extern int RandomNumber(IntPtr model, int min, int max);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_model_lookup_faction_by_cqi")]
		internal static extern IntPtr LookupFactionByCQI(IntPtr model, uint cqi);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_model_lookup_character_by_cqi")]
		internal static extern IntPtr LookupCharacterByCQI(IntPtr model, uint cqi);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_model_script_event_nexus")]
		internal static extern IntPtr EventNexus(IntPtr model);
	}

	[SuppressUnmanagedCodeSecurity]
	internal static class NativeCampaignEventNexus
	{
		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_add_script_faction_end_turn_listener")]
		internal static extern void RegisterFactionEndTurnListener(IntPtr model, IntPtr callback, bool add);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "campaign_add_script_faction_start_turn_listener")]
		internal static extern void RegisterFactionStartTurnListener(IntPtr model, IntPtr callback, bool add);
	}

	[SuppressUnmanagedCodeSecurity]
	internal static class NativeFaction
	{
		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "faction_faction_leader")]
		internal static extern IntPtr FactionLeader(IntPtr faction);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "faction_faction_name")]
		internal static extern IntPtr Name(IntPtr faction);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "faction_treasury")]
		internal static extern int Treasury(IntPtr faction);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "faction_mod_treasury")]
		internal static extern int ModTreasury(IntPtr faction, int mod);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "faction_cqi")]
		internal static extern uint CQI(IntPtr character);
	}

	internal static class NativeCharacter
	{
		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "character_faction")]
		internal static extern IntPtr Faction(IntPtr character);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "character_name")]
		internal static extern IntPtr Name(IntPtr character);

		[DllImport(NativeCampaign.CampaignLibraryPath, EntryPoint = "character_cqi")]
		internal static extern uint CQI(IntPtr character);
	}

	internal static class NativeCampaign
	{
#if WINDOWS
	#if DEBUG
		internal const string CampaignLibraryPath = "CampaignScriptInterface.Debug.dll";
	#else
		internal const string CampaignLibraryPath = "CampaignScriptInterface.Release.dll";
#endif

#else
	internal const string CampaignLibraryPath = "CampaignScriptInterface.so";
	
#endif
	}
}
