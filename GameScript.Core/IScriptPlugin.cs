﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace GameScript.Core
{
	/// <summary>
	/// External Interface for Script Plugins
	/// All libraries in the "Scripts" directory are scanned for types that implement this interface
	/// </summary>
	public interface IScriptPlugin
	{
		/// <summary>
		/// Plugin Name
		/// </summary>
		string Name { get; }

		/// <summary>
		/// Plugin Developer
		/// </summary>
		string Developer { get; }

		/// <summary>
		/// Called once during game startup
		/// </summary>
		/// <param name="model">This would be a model access object or something similar in a real implementation</param>
		void Initialise(CampaignModel model); //Just using the model here in lieu of a higher level interface
	}
}
