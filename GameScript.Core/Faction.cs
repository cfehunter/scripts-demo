using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GameScript.Core.Native;

namespace GameScript.Core
{
	using NativeFactionPtr = IntPtr;
	using NativeCharacterPtr = IntPtr;
	using NativeStringPtr = IntPtr;

	/// <summary>
	/// See Faction
	/// </summary>
	/// <see cref="Faction"/>
	public interface IReadOnlyFaction
	{
		/// <summary>
		/// Faction Name
		/// </summary>
		string             Name { get; }

		/// <summary>
		/// Readonly Faction Leader
		/// </summary>
		IReadOnlyCharacter FactionLeader { get; }

		/// <summary>
		/// Faction treasury
		/// </summary>
		int                Treasury { get; }

		/// <summary>
		/// CQI of this faction
		/// </summary>
		uint CQI { get; }

		/// <summary>
		/// Wether this character is valid
		/// </summary>
		bool Valid { get; }
	}

	/// <summary>
	/// Faction Interface
	/// </summary>
	public struct Faction
		: IReadOnlyFaction
	{
		internal Faction(NativeFactionPtr native_ptr)
		{
			m_native_ptr = native_ptr;

			if (m_native_ptr != IntPtr.Zero)
			{
				CQI = NativeFaction.CQI(m_native_ptr);
			}
			else
			{
				CQI = uint.MaxValue;
			}
		}

		/// <summary>
		/// Faction Name
		/// </summary>
		public string Name
		{
			get { return Valid ? Marshal.PtrToStringAnsi(NativeFaction.Name(m_native_ptr)) : string.Empty; }
		}

		/// <summary>
		/// Character that leads this faction
		/// </summary>
		public Character FactionLeader
		{
			get { return Valid ? new Character(NativeFaction.FactionLeader(m_native_ptr)) : default(Character); }
		}

		/// <summary>
		/// Current treasury of this faction. May be set directly
		/// </summary>
		public int Treasury
		{
			get { return Valid ? NativeFaction.Treasury(m_native_ptr) : int.MinValue; }
			set { if (Valid) { int mod = value - NativeFaction.Treasury(m_native_ptr); NativeFaction.ModTreasury(m_native_ptr, mod); } }
		}

		/// <summary>
		/// CQI of this faction
		/// </summary>
		public uint CQI { get; private set; }

		/// <summary>
		/// Wether this character is valid
		/// </summary>
		public bool Valid => m_native_ptr != IntPtr.Zero;

		#region Equality Operators
		/// <summary>
		/// Equals Faction
		/// </summary>
		/// <param name="lhs">LHS Faction</param>
		/// <param name="rhs">RHS Faction</param>
		/// <returns></returns>
		public static bool operator==(Faction lhs, Faction rhs)
		{
			return lhs.m_native_ptr == rhs.m_native_ptr;
		}

		/// <summary>
		/// Not equal to Faction
		/// </summary>
		/// <param name="lhs">LHS Faction</param>
		/// <param name="rhs">RHS Faction</param>
		/// <returns></returns>
		public static bool operator !=(Faction lhs, Faction rhs)
		{
			return lhs.m_native_ptr != rhs.m_native_ptr;
		}

		/// <summary>
		/// Object Equality
		/// </summary>
		/// <param name="obj">Object to test. Only true if it's a faction</param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			return obj is Faction && ((Faction)obj) == this;
		}

		//Can't do this in the actual game! We would want to just use the CQI here

		/// <summary>
		/// Hash Code. Based on CQI
		/// </summary>
		/// <returns>Hash Code</returns>
		public override int GetHashCode()
		{
			return m_native_ptr.GetHashCode();
		}
		#endregion

		#region IReadOnlyFaction
		IReadOnlyCharacter IReadOnlyFaction.FactionLeader => FactionLeader;
		#endregion

		IntPtr m_native_ptr;
	}

}
