﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

using GameScript.Core.Native;

namespace GameScript.Core
{
	using NativeCharacterPtr = IntPtr;
	using NativeFactionPtr = IntPtr;
	using NativeStringPtr = IntPtr;

	/// <summary>
	/// Read Only Character Interface
	/// </summary>
	/// <see cref="Character"/>
	public interface IReadOnlyCharacter
	{
		/// <summary>
		/// Character Name
		/// </summary>
		string           Name { get; }

		/// <summary>
		/// Faction this character currently serves
		/// </summary>
		IReadOnlyFaction Faction { get; }

		/// <summary>
		/// Character CQI
		/// </summary>
		uint CQI { get; }
	}

	/// <summary>
	/// Character Interface
	/// </summary>
	public struct Character
		: IReadOnlyCharacter
	{
		internal Character(NativeCharacterPtr native_ptr)
		{
			m_native_ptr = native_ptr;

			if (m_native_ptr != IntPtr.Zero)
			{
				CQI = NativeCharacter.CQI(m_native_ptr);
			}
			else
			{
				CQI = uint.MaxValue;
			}
		}

		/// <summary>
		/// Name of the character
		/// </summary>
		public string Name
		{
			get { return Valid ? Marshal.PtrToStringAnsi(NativeCharacter.Name(m_native_ptr)) : string.Empty; }
		}

		/// <summary>
		/// Faction the character currently serves
		/// </summary>
		public Faction Faction
		{
			get { return Valid ? new Faction(NativeCharacter.Faction(m_native_ptr)) : default(Faction); }
		}

		/// <summary>
		/// Command Queue Index of this character
		/// </summary>
		public uint CQI { get; private set; }


		/// <summary>
		/// Wether this faction is valid
		/// </summary>
		public bool Valid => m_native_ptr != IntPtr.Zero;
		//We can either guard this here, as I have, or we can guard it in the C++ interface.
		//The Game will terminate and detach itself from the debugger if any of the C++ accessing properties crash internally
		//When the type is being inspected

		#region Equality Operators
		/// <summary>
		/// Character Equality
		/// </summary>
		/// <param name="lhs">LHS character</param>
		/// <param name="rhs">RHS character</param>
		/// <returns></returns>
		public static bool operator ==(Character lhs, Character rhs)
		{
			return lhs.m_native_ptr == rhs.m_native_ptr;
		}

		/// <summary>
		/// Character InEquality
		/// </summary>
		/// <param name="lhs">LHS character</param>
		/// <param name="rhs">RHS character</param>
		/// <returns></returns>
		public static bool operator !=(Character lhs, Character rhs)
		{
			return lhs.m_native_ptr != rhs.m_native_ptr;
		}

		/// <summary>
		/// Character Object Equality
		/// </summary>
		/// <param name="obj">Object to test. Must be a character to be true</param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			return obj is Character && ((Character)obj) == this;
		}

		//Can't do this in the actual game! We would want to just use the CQI here

		/// <summary>
		/// Hash Code Based on CQI
		/// </summary>
		/// <returns>Hash value</returns>
		public override int GetHashCode()
		{
			return m_native_ptr.GetHashCode();
		}
		#endregion

		#region IReadOnlyCharacter
		IReadOnlyFaction IReadOnlyCharacter.Faction => Faction;
		#endregion

		NativeCharacterPtr m_native_ptr;
	}
}
