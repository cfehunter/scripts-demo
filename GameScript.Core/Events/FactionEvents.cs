﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using GameScript.Core.Native;

namespace GameScript.Core.Events
{
	using NativeFactionPtr = IntPtr;

	/// <summary>
	/// Triggered when a faction ends turn. Provided by the campaign
	/// </summary>
	public struct FactionEndTurn
	{
		[StructLayout(LayoutKind.Sequential)]
		internal struct NativeEvent
		{
			public NativeFactionPtr faction;
		}

		internal FactionEndTurn(NativeEvent native_repr)
		{
			Faction = new Faction(native_repr.faction);
		}

		/// <summary>
		/// Faction that has just ended their turn
		/// </summary>
		public Faction Faction;
	}

	internal class FactionEndTurnEventHandler
		: NativeEventHandler<FactionEndTurn>
	{
		internal FactionEndTurnEventHandler(CampaignEventNexus provider)
			: base(provider, NativeCampaignEventNexus.RegisterFactionEndTurnListener)
		{ }

		protected override FactionEndTurn CreateEventFromNative(IntPtr ptr)
		{
			FactionEndTurn.NativeEvent native_evt = Marshal.PtrToStructure<FactionEndTurn.NativeEvent>(ptr);
			return new FactionEndTurn(native_evt);
		}
	}

	/// <summary>
	/// Triggered when a faction starts their turn. Provided by the campaign
	/// </summary>
	public struct FactionStartTurn
	{
		[StructLayout(LayoutKind.Sequential)]
		internal struct NativeEvent
		{
			public NativeFactionPtr faction;
		}

		internal FactionStartTurn(NativeEvent native_repr)
		{
			Faction = new Faction(native_repr.faction);
		}

		/// <summary>
		/// Faction that has just started their turn
		/// </summary>
		public Faction Faction;
	}

	internal class FactionStartTurnEventHandler
		: NativeEventHandler<FactionStartTurn>
	{
		internal FactionStartTurnEventHandler(CampaignEventNexus provider)
			: base(provider, NativeCampaignEventNexus.RegisterFactionStartTurnListener)
		{ }

		protected override FactionStartTurn CreateEventFromNative(IntPtr ptr)
		{
			FactionStartTurn.NativeEvent native_event = Marshal.PtrToStructure<FactionStartTurn.NativeEvent>(ptr);
			return new FactionStartTurn(native_event);
		}
	}


}
