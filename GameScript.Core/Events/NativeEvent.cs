﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GameScript.Core.Native;

namespace GameScript.Core.Events
{
	internal delegate void RawNativeEventRegistrar(IntPtr provider, IntPtr callback, bool add);
	internal delegate void RawNativeEventCallback(IntPtr native_ptr);

	internal interface INativeEventProvider
	{
		IntPtr NativePtr { get; }
	}

	/// <summary>
	/// Event Args with no sender
	/// </summary>
	/// <typeparam name="T">Content Type</typeparam>
	/// <param name="args">Content Type Instance</param>
	public delegate void NoSenderEventArgs<T>(T args);

	internal abstract class NativeEventHandler<T>
		: IDisposable
	{
		internal NativeEventHandler(INativeEventProvider provider, RawNativeEventRegistrar subscriber)
		{
			m_provider = provider.NativePtr;
			m_callback = HandleNativeCallback;
			m_subscriber = subscriber;
			subscriber(m_provider, Marshal.GetFunctionPointerForDelegate(m_callback), true);
		}

		public void Dispose()
		{
			if (m_callback != null)
			{
				m_subscriber(m_provider, Marshal.GetFunctionPointerForDelegate(m_callback), false);
				m_callback = null;
			}
		}

		internal event NoSenderEventArgs<T> OnInvoked;

		protected void HandleNativeCallback(IntPtr native_event)
		{
			OnInvoked?.Invoke(CreateEventFromNative(native_event));
		}

		protected abstract T CreateEventFromNative(IntPtr native_event);

		private IntPtr                   m_provider;
		private RawNativeEventCallback   m_callback;
		private RawNativeEventRegistrar m_subscriber;
	}
}
