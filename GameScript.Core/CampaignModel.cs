﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;

using GameScript.Core.Events;
using GameScript.Core.Native;

namespace GameScript.Core
{
	using NativeFactionPtr = IntPtr;
	using NativeModelPtr = IntPtr;

	//We have a choice to make with making in read only interface.
	//We can either implement it as I have here, which allows us to write functions that consume a read only interface, but also allows you to explicitly cast to the base object (CampaignModel in this case).
	//or we can use a read only wrapper to stop consumers of the system from casting to the base object from the read only interface


	/// <summary>
	/// Read only campaign model interface
	/// </summary>
	/// <see cref="CampaignModel"/>
	public interface IReadOnlyCampaignModel
	{
		/// <summary>
		/// Faction whos turn it currently is
		/// </summary>
		IReadOnlyFaction              CurrentTurnFaction { get; }

		/// <summary>
		/// All factions currently in the campaign. Including dead ones
		/// </summary>
		IEnumerable<IReadOnlyFaction> Factions { get; }

		/// <summary>
		/// Central campaign event access
		/// </summary>
		CampaignEventNexus            EventNexus { get; }
	}
	
	/// <summary>
	/// Campaign Model Interface
	/// </summary>
	public sealed class CampaignModel
		: IReadOnlyCampaignModel
	{
		internal CampaignModel(IntPtr native_ptr)
		{
			m_native_ptr = native_ptr;
			EventNexus = new CampaignEventNexus(NativeCampaignModel.EventNexus(m_native_ptr));

			NativeFactionPtr native_factions_array_ptr = NativeCampaignModel.Factions(m_native_ptr, out uint size);

			//Return value is an array of pointers, so the initial pointer needs to be incremented and then derferenced to actually get at the value
			m_factions = new List<Faction>((int)size);
			for (int i = 0; i < size; ++i)
			{
				NativeFactionPtr native_faction_ptr = Marshal.PtrToStructure<IntPtr>(IntPtr.Add(native_factions_array_ptr, i * IntPtr.Size));
				m_factions.Add(new Faction(native_faction_ptr));
			}
		}

		/// <summary>
		/// Faction whos turn it currently is
		/// </summary>
		public Faction                CurrentTurnFaction => new Faction(NativeCampaignModel.CurrentTurnFaction(m_native_ptr));

		/// <summary>
		/// All factions currently present in the campaign. Including those that are dead
		/// </summary>
		public IReadOnlyList<Faction> Factions => m_factions;

		/// <summary>
		/// Event provider for the campaign
		/// </summary>
		public CampaignEventNexus     EventNexus { get; private set; }


		/// <summary>
		/// Attempt to lookup a faction by command queue index
		/// </summary>
		/// <param name="cqi">Faction CQI</param>
		/// <returns>Faction with CQI. May be null if CQI is invalid</returns>
		public Faction LookupFactionByCQI(uint cqi)
		{
			return new Faction(NativeCampaignModel.LookupFactionByCQI(m_native_ptr, cqi));
		}

		/// <summary>
		/// Attempt to lookup a character by command queue index
		/// </summary>
		/// <param name="cqi">Character CQI</param>
		/// <returns>Character with CQI. May be null if CQI is invalid</returns>
		public Character LookupCharacterByCQI(uint cqi)
		{
			return new Character(NativeCampaignModel.LookupCharacterByCQI(m_native_ptr, cqi));
		}

		/// <summary>
		/// Generate a random number using the sync safe generator.
		/// It's important that you use this (and don't use a local random number generator) so that the game remains in sync during multiplayer
		/// </summary>
		/// <param name="min">Minimum Value</param>
		/// <param name="max">Maximum Value</param>
		/// <returns>Random result between min and max</returns>
		public int Random(int min, int max)
		{
			return NativeCampaignModel.RandomNumber(m_native_ptr, min, max);
		}

		#region IReadOnlyCampaignModel
		IReadOnlyFaction              IReadOnlyCampaignModel.CurrentTurnFaction => CurrentTurnFaction;
		IEnumerable<IReadOnlyFaction> IReadOnlyCampaignModel.Factions => Factions.Cast<IReadOnlyFaction>();
		#endregion

		//Campaign model is a class, so we can store events locally in the class
		//Faction and other structs that are just wrappers around the native pointer, require more care
		List<Faction>                m_factions;
		NativeModelPtr               m_native_ptr;
	}
}
