﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;

namespace GameScript.Core
{
	static class Bootstrap
	{
#if DEBUG
		internal const string CampaignLibraryPath = "CampaignScriptInterface.Debug.dll";
#else
		internal const string CampaignLibraryPath = "CampaignScriptInterface.Release.dll";
#endif
		internal const string ScriptLibraryExtension = ".dll";

		static CampaignModel          active_campaign_model;
		static List<IScriptPlugin>    loaded_plugins;
		static List<Type>             collect_script_plugins()
		{
			List<Type> plugins = new List<Type>();

			string current_directory = Directory.GetCurrentDirectory();
			string scripts_directory = Path.Combine(current_directory, "Scripts");

			if (!Directory.Exists(scripts_directory))
			{
				//TODO: Hook asserts
				return plugins;
			}

			foreach (string file in Directory.EnumerateFiles(scripts_directory))
			{
				if (file.EndsWith(ScriptLibraryExtension, StringComparison.InvariantCultureIgnoreCase))
				{
					try
					{
						// /!\IMPORTANT/!\ .NET Core doesn't support multiple app domains, it'll just throw an unimplemented exception if you try to make one
						// This means that plugins can take down the process from managed code, but the same is true of LUA
						Assembly scripts_library = AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(file));
						foreach (Type t in scripts_library.GetTypes())
						{
							if (t.IsClass && t.IsPublic && !t.IsAbstract && typeof(IScriptPlugin).IsAssignableFrom(t))
							{
								plugins.Add(t);
							}
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine($"Exception: while loading library {Path.GetFileName(file)}\n{ex.ToString()}");
					}
				}
			}

			return plugins;
		}

		static void bootstrap(IntPtr campaign_model_ptr)
		{
			active_campaign_model = new CampaignModel(campaign_model_ptr);

			Console.WriteLine("Script Interface Bootstrapping");

			//We could use something like the Plugin API https://docs.microsoft.com/en-us/dotnet/framework/add-ins/walkthrough-create-extensible-app
			//But it seems a little overkill for what we're trying to do here. So I'm just going to use reflection to load types that implement an interface
			//We can also do things like create/destroy plugins as we go between game scopes and extend IScriptPlugin to accomodate more metadata
			List<Type> plugins = collect_script_plugins();
			Console.WriteLine($"Found {plugins.Count} viable script plugins");

			Console.WriteLine($"Creating script plugins");
			loaded_plugins = new List<IScriptPlugin>(plugins.Count);
			foreach (Type plugin_type in plugins)
			{
				try
				{
					IScriptPlugin plugin = (IScriptPlugin)Activator.CreateInstance(plugin_type);
					if (plugin != null)
					{
						Console.WriteLine($"Instantiated Script Plugin: {plugin.Name} By: {plugin.Developer}");
						loaded_plugins.Add(plugin);
					}
					else
					{
						Console.ForegroundColor = ConsoleColor.Red;
						Console.WriteLine($"Failed to Instantiate Plugin: {plugin_type.Name}. Please make sure it implements IScriptPlugin and has a default constructor");
						Console.ResetColor();
					}
				} catch (Exception ex)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine($"Plugin Instantiation Exception: {plugin_type.Name}. Exception: {ex.ToString()}");
					Console.ResetColor();
				}
			}

			Console.WriteLine($"Initialising script plugins");
			foreach (IScriptPlugin plugin in loaded_plugins)
			{
				plugin.Initialise(active_campaign_model);
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine($"{plugin.Name} initialised");
			}

			Console.ResetColor();
		}

		static void benchmark(int amount)
		{
			for (int i = 0; i < amount; ++i)
			{
				active_campaign_model.Random(0, 1000);
			}
		}

		static int benchmark_fib(int amount)
		{
			//Don't need to return, but it'll stop it optimising out the operation
			int a = 0;
			int b = 1;

			for (int i = 0; i < amount; ++i)
			{
				int temp = a;
				a = b;
				b += temp;
			}

			Console.WriteLine($"C# Result:{a}");
			return a;
		}
	}
}
