﻿using System;
using System.Collections.Generic;

using GameScript.Core.Events;
using GameScript.Core.Native;

namespace GameScript.Core
{
	/// <summary>
	/// Provides access to events from the native campaign model
	/// Accessible from the read only interface as well
	/// </summary>
	public class CampaignEventNexus
		: INativeEventProvider
	{
		internal CampaignEventNexus(IntPtr native_ptr)
		{
			m_native_ptr = native_ptr;

			//Dont initialise these inline with their definition.
			//They'll get constructed before the pointer is set
			m_faction_end_turn_event   = new FactionEndTurnEventHandler(this);
			m_faction_start_turn_event = new FactionStartTurnEventHandler(this);
		}

		/// <summary>
		/// Triggered when a faction ends their turn
		/// </summary>
		public event NoSenderEventArgs<FactionEndTurn> OnFactionEndTurn
		{
			add { m_faction_end_turn_event.OnInvoked += value; }
			remove { m_faction_end_turn_event.OnInvoked -= value; }
		}

		/// <summary>
		/// Triggered when a faction starts their turn
		/// </summary>
		public event NoSenderEventArgs<FactionStartTurn> OnFactionStartTurn
		{
			add { m_faction_start_turn_event.OnInvoked += value; }
			remove { m_faction_start_turn_event.OnInvoked -= value; }
		}

		IntPtr INativeEventProvider.NativePtr => m_native_ptr;

		FactionEndTurnEventHandler   m_faction_end_turn_event;
		FactionStartTurnEventHandler m_faction_start_turn_event;
		private IntPtr               m_native_ptr;
	}
}
