#include "CampaignModel.h"

#include "Character.h"
#include "Faction.h"
#include "ScriptEvents.h"

namespace CAMPAIGN
{

	CAMPAIGN_MODEL::CAMPAIGN_MODEL()
		: m_script_event_nexus()
		, m_factions()
		, m_characters()
		, m_current_turn_faction()
		, m_round_number(0)
	{
		m_factions = 
		{
			 new FACTION(0, *this, "FACTION A", "FACTION LEADER A")
			,new FACTION(1, *this, "FACTION B", "FACTION LEADER B")
			,new FACTION(2, *this, "FACTION C", "FACTION LEADER C")
			,new FACTION(3, *this, "FACTION D", "FACTION LEADER D")
			,new FACTION(4, *this, "FACTION E", "FACTION LEADER E")
			,new FACTION(5, *this, "FACTION F", "FACTION LEADER F")
		};

		m_current_turn_faction = m_factions.begin();
	}

	CAMPAIGN_MODEL::~CAMPAIGN_MODEL()
	{
		for (FACTION* faction : m_factions)
		{
			delete faction;
		}

		for (CHARACTER* character : m_characters)
		{
			delete character;
		}
	}

	FACTION& CAMPAIGN_MODEL::current_turn_faction() const
	{
		return **m_current_turn_faction;
	}

	void CAMPAIGN_MODEL::start_turn()
	{
		m_script_event_nexus.notify_script_event(FACTION_START_TURN(**m_current_turn_faction));
		(*m_current_turn_faction)->start_turn();
	}

	void CAMPAIGN_MODEL::end_turn()
	{
		m_script_event_nexus.notify_script_event(FACTION_END_TURN(**m_current_turn_faction));

		(*m_current_turn_faction)->end_turn();
		++m_current_turn_faction;

		if (m_current_turn_faction == m_factions.end())
		{
			m_current_turn_faction = m_factions.begin();
		}

		start_turn();
	}

	int32 CAMPAIGN_MODEL::random_number(int32 min, int32 max)
	{
		max = std::max(min, max);
		return  min + (std::rand() % (max - min));
	}

	CHARACTER& CAMPAIGN_MODEL::make_character(FACTION & faction, std::string&& name)
	{
		m_characters.emplace_back(new CHARACTER(static_cast<unsigned int>(m_characters.size()), faction, std::move(name)));
		return *m_characters.back();
	}

	CHARACTER* CAMPAIGN_MODEL::lookup_character_by_cqi(unsigned int index)
	{
		if (index < m_characters.size())
		{
			return m_characters[index];
		}

		return nullptr;
	}

	FACTION * CAMPAIGN_MODEL::lookup_faction_by_cqi(unsigned int index)
	{
		if (index < m_factions.size())
		{
			return m_factions[index];
		}

		return nullptr;
	}

} //namespace CAMPAIGN