#include "Character.h"

namespace CAMPAIGN
{

	CHARACTER::CHARACTER(unsigned int cqi, FACTION& faction, std::string&& name)
		: m_faction(faction)
		, m_name(std::move(name))
		, m_cqi(cqi)
	{ }

	void CHARACTER::start_turn()
	{
	}

	void CHARACTER::end_turn()
	{
	}

} //namespace CAMPAIGN