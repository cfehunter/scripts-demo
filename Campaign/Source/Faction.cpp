#include "Faction.h"

#include "CampaignModel.h"

namespace CAMPAIGN
{

	FACTION::FACTION(unsigned int cqi, CAMPAIGN_MODEL& model, std::string&& name, std::string&& faction_leader_name)
		: m_faction_leader(model.make_character(*this, std::move(faction_leader_name)))
		, m_faction_name(std::move(name))
		, m_cqi(cqi)
		, m_treasury(0)
	{
	}

	void FACTION::start_turn()
	{
	}

	void FACTION::end_turn()
	{
	}

} //namespace CAMPAIGN