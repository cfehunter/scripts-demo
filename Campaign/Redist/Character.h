#pragma once

#ifndef _CHARACTER_H_INCLUDED_
#define _CHARACTER_H_INCLUDED_

#include <string>

namespace CAMPAIGN
{
	class FACTION;

	class CHARACTER
	{
	public:
		CHARACTER(unsigned int cqi, FACTION& faction, std::string&& name);

		unsigned int cqi() const { return m_cqi; }
		void start_turn();
		void end_turn();

		FACTION&           faction() { return m_faction; }
		const std::string& name() const { return m_name; }

	private:
		FACTION&     m_faction;
		std::string  m_name;
		unsigned int m_cqi;
	};

} //namespace CAMPAIGN

#endif //_CHARACTER_H_INCLUDED_