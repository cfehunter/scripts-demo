#pragma once

#ifndef _FACTION_H_INCLUDED_
#define _FACTION_H_INCLUDED_

#include <string>

namespace CAMPAIGN
{
	class CAMPAIGN_MODEL;
	class CHARACTER;

	class FACTION
	{
	public:
		FACTION(unsigned int cqi, CAMPAIGN_MODEL& model, std::string&& name, std::string&& faction_leader_name);

		void start_turn();
		void end_turn();

		unsigned int       cqi() const { return m_cqi; }
		CHARACTER&         faction_leader() { return m_faction_leader; }
		const std::string& faction_name() const { return m_faction_name; }
		int                treasury() const { return m_treasury; }
		void               modify_treasury(int mod) { m_treasury += mod; }
	private:
		CHARACTER&   m_faction_leader;
		std::string  m_faction_name;
		unsigned int m_cqi;
		int          m_treasury;
	};

} //namespace CAMPAIGN

#endif //_FACTION_H_INCLUDED_