#pragma once
#ifndef _SCRIPT_EVENTS_H_INCLUDED_
#define _SCRIPT_EVENTS_H_INCLUDED_

namespace CAMPAIGN
{
	class FACTION;

	struct FACTION_END_TURN
	{
		explicit FACTION_END_TURN(const FACTION& faction)
			: faction(faction)
		{ }

		const FACTION& faction;
	};

	struct FACTION_START_TURN
	{
		explicit FACTION_START_TURN(const FACTION& faction)
			: faction(faction)
		{}

		const FACTION& faction;
	};

} //namespace CAMPAIGN

#endif //_SCRIPT_EVENTS_H_INCLUDED_