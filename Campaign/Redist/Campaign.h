#pragma once

#ifndef __CAMPAIGN_H_INCLUDED__
#define __CAMPAIGN_H_INCLUDED__

//Windows
#ifdef _WIN32

#ifdef CAMPAIGN_LIB
#define CAMPAIGN_API __declspec(dllexport)
#else
#define CAMPAIGN_API __declspec(dllimport)
#endif

//Not Windows
#else
#define CAMPAIGN_API
#endif

using int8   = char;
using int16  = short;
using int32  = int;
using int64  = long long;
using card8  = unsigned char;
using card16 = unsigned short;
using card32 = unsigned int;
using card64 = unsigned long long;

#endif //__CAMPAIGN_H_INCLUDED__
