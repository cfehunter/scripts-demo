#pragma once

#ifndef _CAMPAIGN_MODEL_H_INCLUDED_
#define _CAMPAIGN_MODEL_H_INCLUDED_

#include <string>
#include <vector>

#include "Campaign.h"
#include "ScriptEventReporter.h"

namespace CAMPAIGN
{
	class CHARACTER;
	class FACTION;

	struct FACTION_END_TURN;
	struct FACTION_START_TURN;

	//This needs to be exposed to C, so it needs to be a wrapper rather than a typedef of the variadic nexus
	class CAMPAIGN_SCRIPT_EVENT_NEXUS
		: public COMMON::SCRIPT_EVENT_NEXUS<FACTION_END_TURN, FACTION_START_TURN>
	{ };

	class CAMPAIGN_MODEL
	{
	public:
		using FACTIONS = std::vector<FACTION*>;
		using CHARACTERS = std::vector<CHARACTER*>;

		CAMPAIGN_MODEL();
		~CAMPAIGN_MODEL();

		const FACTIONS&              factions() const { return m_factions; }
		FACTION&                     current_turn_faction() const;
		void                         start_turn();
		void                         end_turn();

		int32                        random_number(int32 min, int32 max);
		CHARACTER&                   make_character(FACTION& faction, std::string&& name);
		CHARACTER*                   lookup_character_by_cqi(unsigned int cqi);

		FACTIONS::iterator           factions_begin() { return m_factions.begin(); }
		FACTIONS::iterator           factions_end() { return m_factions.end(); }
		FACTION*                     lookup_faction_by_cqi(unsigned int index);
		CAMPAIGN_SCRIPT_EVENT_NEXUS& event_nexus() { return m_script_event_nexus; }

	private:
		CAMPAIGN_SCRIPT_EVENT_NEXUS m_script_event_nexus;
		FACTIONS                    m_factions;
		CHARACTERS                  m_characters;
		FACTIONS::iterator          m_current_turn_faction;
		size_t                      m_round_number;
	};
}

#endif //_CAMPAIGN_MODEL_H_INCLUDED_