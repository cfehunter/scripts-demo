#pragma once

#ifndef _CHARACTER_INTERFACE_H_INCLUDED_
#define _CHARACTER_INTERFACE_H_INCLUDED_

#include "CampaignScriptInterface.h"

namespace CAMPAIGN
{
	class CHARACTER;
	class FACTION;

	extern "C"
	{
		CAMPAIGN_SCRIPT_API unsigned int character_cqi(CHARACTER& character);
		CAMPAIGN_SCRIPT_API FACTION&     character_faction(CHARACTER& character);
		CAMPAIGN_SCRIPT_API const char*  character_name(CHARACTER& character);
	}
}

#endif //_CHARACTER_INTERFACE_H_INCLUDED_
