#pragma once

#ifndef _FACTION_INTERFACE_H_INCLUDED_
#define _FACTION_INTERFACE_H_INCLUDED_

#include "CampaignScriptInterface.h"

namespace CAMPAIGN
{
	class CHARACTER;
	class FACTION;

	extern "C"
	{
		CAMPAIGN_SCRIPT_API unsigned int faction_cqi(FACTION& faction);
		CAMPAIGN_SCRIPT_API CHARACTER&   faction_faction_leader(FACTION& faction);
		CAMPAIGN_SCRIPT_API const char*  faction_faction_name(FACTION& faction);
		CAMPAIGN_SCRIPT_API int          faction_treasury(FACTION& faction);
		CAMPAIGN_SCRIPT_API void         faction_mod_treasury(FACTION& faction, int mod);
	}

} //namespace CAMPAIGN

#endif //_FACTION_INTERFACE_H_INCLUDED_