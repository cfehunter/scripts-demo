#pragma once

#ifndef _CAMPAIGN_SCRIPT_INTERFACE_H_INCLUDED_
#define _CAMPAIGN_SCRIPT_INTERFACE_H_INCLUDED_

//Windows
#ifdef _WIN32

#ifdef CAMPAIGN_SCRIPT_INTERFACE_LIB
#define CAMPAIGN_SCRIPT_API __declspec(dllexport)
#else
#define CAMPAIGN_SCRIPT_API __declspec(dllimport)
#endif

//Not Windows
#else
#define CAMPAIGN_SCRIPT_API
#endif

#endif //_CAMPAIGN_SCRIPT_INTERFACE_H_INCLUDED_
