#pragma once

#ifndef _CAMPAIGN_MODEL_INTERFACE_H_INCLUDED_
#define _CAMPAIGN_MODEL_INTERFACE_H_INCLUDED_

#include "CampaignScriptInterface.h"
#include "ScriptEventReporter.h"

namespace CAMPAIGN
{
	class CAMPAIGN_MODEL;
	class CAMPAIGN_SCRIPT_EVENT_NEXUS;
	class CHARACTER;
	class FACTION;

	struct FACTION_END_TURN;
	struct FACTION_START_TURN;

	extern "C"
	{
		CAMPAIGN_SCRIPT_API FACTION**                    campaign_model_factions(CAMPAIGN_MODEL* model, unsigned int& size);
		CAMPAIGN_SCRIPT_API FACTION*                     campaign_model_current_turn_faction(CAMPAIGN_MODEL* model);
		CAMPAIGN_SCRIPT_API int                          campaign_model_random_number(CAMPAIGN_MODEL* model, int min, int max);
		CAMPAIGN_SCRIPT_API FACTION*                     campaign_model_lookup_faction_by_cqi(CAMPAIGN_MODEL* model, unsigned int cqi);
		CAMPAIGN_SCRIPT_API CHARACTER*                   campaign_model_lookup_character_by_cqi(CAMPAIGN_MODEL* model, unsigned int cqi);
		CAMPAIGN_SCRIPT_API CAMPAIGN_SCRIPT_EVENT_NEXUS* campaign_model_script_event_nexus(CAMPAIGN_MODEL* model);
	}

	//campaign_model_add_script_faction_end_turn_listener;
	//campaign_model_remove_script_faction_end_turn_listener;
	DECLARE_SCRIPT_EVENT_REPORTER(CAMPAIGN_SCRIPT_EVENT_NEXUS, campaign, FACTION_END_TURN, faction_end_turn)
	DECLARE_SCRIPT_EVENT_REPORTER(CAMPAIGN_SCRIPT_EVENT_NEXUS, campaign, FACTION_START_TURN, faction_start_turn)
}

#endif //_CAMPAIGN_MODEL_INTERFACE_H_INCLUDED_