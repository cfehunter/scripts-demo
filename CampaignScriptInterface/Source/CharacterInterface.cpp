#include "CharacterInterface.h"

#include "Character.h"

namespace CAMPAIGN
{
	extern "C"
	{
		unsigned int character_cqi(CHARACTER& character)
		{
			return character.cqi();
		}

		FACTION& character_faction(CHARACTER& character)
		{
			return character.faction();
		}

		const char* character_name(CHARACTER& character)
		{
			return character.name().data();
		}
	}
}