#include "FactionInterface.h"

#include "Faction.h"

namespace CAMPAIGN
{
	extern "C"
	{
		unsigned int faction_cqi(FACTION& faction)
		{
			return faction.cqi();
		}

		CHARACTER& faction_faction_leader(FACTION& faction)
		{
			return faction.faction_leader();
		}

		const char* faction_faction_name(FACTION& faction)
		{
			return faction.faction_name().data();
		}

		int faction_treasury(FACTION& faction)
		{
			return faction.treasury();
		}

		void faction_mod_treasury(FACTION& faction, int mod)
		{
			faction.modify_treasury(mod);
		}
	}
} //namespace CAMPAIGN