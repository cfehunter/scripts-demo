#include <string>

#include "CampaignModelInterface.h"

#include "CampaignModel.h"
#include "ScriptEventReporter.h"


namespace CAMPAIGN
{
	extern "C"
	{
		//Forgive the hack for now. I need direct access to the memory of the factions array to expose it in this way
		//In theory we should be able to expose an enumerable type, but I have some concerns about the amount of context
		//switches between managed and unmanaged code that would cause. Not sure if those concerns are valid or not
		FACTION** campaign_model_factions(CAMPAIGN_MODEL* model, unsigned int& size)
		{
			size = static_cast<unsigned int>(model->factions_end() - model->factions_begin());
			return const_cast<CAMPAIGN_MODEL::FACTIONS&>(model->factions()).data();
		}

		FACTION* campaign_model_current_turn_faction(CAMPAIGN_MODEL* model)
		{
			return &model->current_turn_faction();
		}

		int campaign_model_random_number(CAMPAIGN_MODEL* model, int min, int max)
		{
			return model->random_number(min, max);
		}

		FACTION* campaign_model_lookup_faction_by_cqi(CAMPAIGN_MODEL* model, unsigned int cqi)
		{
			return model->lookup_faction_by_cqi(cqi);
		}
		
		CHARACTER* campaign_model_lookup_character_by_cqi(CAMPAIGN_MODEL* model, unsigned int cqi)
		{
			return model->lookup_character_by_cqi(cqi);
		}

		CAMPAIGN_SCRIPT_EVENT_NEXUS* campaign_model_script_event_nexus(CAMPAIGN_MODEL* model)
		{
			return &model->event_nexus();
		}
	}

	DEFINE_SCRIPT_EVENT_REPORTER(CAMPAIGN_SCRIPT_EVENT_NEXUS, campaign, FACTION_END_TURN, faction_end_turn)
	DEFINE_SCRIPT_EVENT_REPORTER(CAMPAIGN_SCRIPT_EVENT_NEXUS, campaign, FACTION_START_TURN, faction_start_turn)
}