using System;
using System.Collections.Generic;
using System.Text;

using GameScript.Core;
using GameScript.Core.Events;

namespace GameScript.ExamplePlugin
{
	//There's no need for this to be static.
	static class FactionTreasuryGiver
	{
		public static void Initialise(CampaignModel model)
		{
			model.EventNexus.OnFactionEndTurn += OnFactionEndTurn;
			m_favoured_faction = model.Factions[model.Random(0, model.Factions.Count - 1)];
			Console.ForegroundColor = ConsoleColor.DarkCyan;
			Console.WriteLine($"Faction Treasury Giver | Selected Faction: {m_favoured_faction.Name}");
			Console.ResetColor();
		}

		static void OnFactionEndTurn(FactionEndTurn evt)
		{
			if (evt.Faction == m_favoured_faction)
			{
				Console.ForegroundColor = ConsoleColor.DarkCyan;
				Console.WriteLine($"Favoured Faction has just finished their turn. Gifting 100 treasury\nBefore: {evt.Faction.Treasury}");
				m_favoured_faction.Treasury += 100;
				Console.WriteLine($"After {evt.Faction.Treasury}\n");
				Console.ResetColor();
			}
			else
			{
				Console.ForegroundColor = ConsoleColor.DarkCyan;
				Console.WriteLine($"Faciton: {evt.Faction.Name} has just finished their turn. Ignoring");
				Console.ResetColor();
			}
		}

		static Faction m_favoured_faction;
	}
}
