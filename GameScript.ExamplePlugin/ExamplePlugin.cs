﻿using System;

using GameScript.Core;
using GameScript.Core.Events;


namespace GameScript.ExamplePlugin
{
	//This is the root of the plugin. You can do whatever you want from here
	public class ExamplePlugin
		: IScriptPlugin
	{
		public string Name => "Example Plugin";
		public string Developer => "Peter Ellsum";


		public void Initialise(CampaignModel model)
		{
			m_model = model;

			//Console.ForegroundColor = ConsoleColor.DarkMagenta;
			//Console.WriteLine("Reading Factions from Read Only Model to test read only interface...");
			//IReadOnlyCampaignModel ro_model = model;
			//foreach (IReadOnlyFaction f in ro_model.Factions)
			//{
			//	Console.WriteLine("Read Only Faction: " + f.Name);
			//}
			//Console.ResetColor();
			FactionTreasuryGiver.Initialise(model);
		}

		CampaignModel m_model;
	}
}
