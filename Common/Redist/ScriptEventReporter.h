#pragma once
#ifndef _SCRIPT_EVENT_REPORTER_H_INCLUDED_
#define _SCRIPT_EVENT_REPORTER_H_INCLUDED_

#include <type_traits>
#include <vector>
#include "Common.h"

namespace COMMON
{
	//The alternative approach to doing this would be to use the hosting interface to create delegates from the library
	//and have the C++ side manage the lifetimes.
	//This makes the host script interface aware that it's interacting with C# though, and that'll be massively unhelpful if we decide to either port
	//to a new language or compile our C# code to native binaries when we ship

	template <typename T>
	class SCRIPT_EVENT_REPORTER
	{
	public:
		using CALLBACK_TYPE = void(*)(const T&);

		SCRIPT_EVENT_REPORTER();

		void notify_script_event(const T& evt);
		void add_script_event_listener(CALLBACK_TYPE cb);
		void remove_script_event_listener(CALLBACK_TYPE cb);

	private:
		std::vector<CALLBACK_TYPE> m_callbacks;
	};

	template <typename... T_EVENTS>
	class SCRIPT_EVENT_NEXUS
		: private SCRIPT_EVENT_REPORTER<T_EVENTS>...
	{
	public:
		//I *think* there's a way to use a variadic using statement in C++17 to avoid the need for these functions
		template <typename T>
		void notify_script_event(const T& evt)
		{
			static_cast<SCRIPT_EVENT_REPORTER<T>&>(*this).notify_script_event(evt);
		}

		template <typename T, typename CALLBACK_TYPE>
		void add_script_event_listener(CALLBACK_TYPE cb)
		{
			static_cast<SCRIPT_EVENT_REPORTER<T>&>(*this).add_script_event_listener(cb);
		}

		template <typename T, typename CALLBACK_TYPE>
		void remove_script_event_listener(CALLBACK_TYPE cb)
		{
			static_cast<SCRIPT_EVENT_REPORTER<T>&>(*this).remove_script_event_listener(cb);
		}
	};
} //namespace COMMON

#define DECLARE_SCRIPT_EVENT_REPORTER(PROVIDER_TYPE, PROVIDER_NAME, EVENT_TYPE, NAME)\
using EVENT_TYPE##_CALLBACK_TYPE = void(*)(const EVENT_TYPE&);\
extern "C"\
{\
	 COMMON_EXPORT void PROVIDER_NAME##_add_script_##NAME##_listener(PROVIDER_TYPE* provider, EVENT_TYPE##_CALLBACK_TYPE cb, bool add);\
}

#define DEFINE_SCRIPT_EVENT_REPORTER(PROVIDER_TYPE, PROVIDER_NAME, EVENT_TYPE, NAME)\
extern "C"\
{\
	void PROVIDER_NAME##_add_script_##NAME##_listener(PROVIDER_TYPE* provider, EVENT_TYPE##_CALLBACK_TYPE cb, bool add)\
	{\
		if (add)\
		{\
			provider->add_script_event_listener<EVENT_TYPE>(cb); \
		}\
		else\
		{\
			provider->remove_script_event_listener<EVENT_TYPE>(cb); \
		}\
	}\
}

#include "ScriptEventReporter.inl"

#endif //_SCRIPT_EVENT_REPORTER_H_INCLUDED_
