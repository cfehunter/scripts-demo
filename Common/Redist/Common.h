#pragma once

#ifndef _COMMON_H_INCLUDED_
#define _COMMON_H_INCLUDED_

#ifdef _WIN32
#define COMMON_EXPORT __declspec(dllexport)
#else
#define COMMON_EXPORT
#endif

#ifdef _WIN32

#ifdef COMMON_LIB
#define COMMON_API __declspec(dllexport)
#else
#define COMMON_API __declspec(dllimport)
#endif

#else
#define COMMON_API
#endif



#endif //_COMMON_H_INCLUDED_
