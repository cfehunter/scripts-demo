#include <algorithm>
#include "ScriptEventReporter.h"


namespace COMMON
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////

	template <typename T>
	SCRIPT_EVENT_REPORTER<T>::SCRIPT_EVENT_REPORTER()
		: m_callbacks()
	{ }

	template <typename T>
	void SCRIPT_EVENT_REPORTER<T>::notify_script_event(const T& evt)
	{
		for (CALLBACK_TYPE& cb : m_callbacks)
		{
			cb(evt);
		}
	}

	template <typename T>
	void SCRIPT_EVENT_REPORTER<T>::add_script_event_listener(CALLBACK_TYPE cb)
	{
		if (std::find(m_callbacks.begin(), m_callbacks.end(), cb) == m_callbacks.end())
		{
			m_callbacks.emplace_back(cb);
		}
	}

	template <typename T>
	void SCRIPT_EVENT_REPORTER<T>::remove_script_event_listener(CALLBACK_TYPE cb)
	{
		m_callbacks.erase(std::remove(m_callbacks.begin(), m_callbacks.end(), cb), m_callbacks.end());
	}

} //namespace COMMON