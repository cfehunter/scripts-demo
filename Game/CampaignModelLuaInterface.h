#pragma once

#ifndef CAMPAIGN_MODEL_LUA_INTERFACE_H_INCLUDED
#define CAMPAIGN_MODEL_LUA_INTERFACE_H_INCLUDED

#include "Lunar.h"

extern "C"
{
	struct lua_State;
}

namespace CAMPAIGN
{
	class CAMPAIGN_MODEL;
}

class CAMPAIGN_LUA_INTERFACE
{
public:
	static const char className[];
	static Lunar<CAMPAIGN_LUA_INTERFACE>::RegType methods[];

	CAMPAIGN_LUA_INTERFACE(lua_State* ls);
	CAMPAIGN_LUA_INTERFACE(CAMPAIGN::CAMPAIGN_MODEL& model);
	~CAMPAIGN_LUA_INTERFACE();
	int random_number(lua_State* ls);

	static int create_and_push_to_stack(lua_State* ls, CAMPAIGN::CAMPAIGN_MODEL& model);

private:
	CAMPAIGN::CAMPAIGN_MODEL* m_model;
};

#endif //CAMPAIGN_MODEL_LUA_INTERFACE_H_INCLUDED
