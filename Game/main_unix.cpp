#ifndef WIN32

#include <array>
#include <filesystem>
#include <iostream>
#include <string>

#include <errno.h>
#include <dlfcn.h>
#include <unistd.h>
#include <cstring>

#include "coreclrhost.h"
#include "CampaignModel.h"

//RAII module freeing
class HMODULE_WRAPPER
{
public:
	HMODULE_WRAPPER(void* handle)
		: m_handle(handle)
	{}

	HMODULE_WRAPPER(const HMODULE_WRAPPER&) = delete;
	HMODULE_WRAPPER(HMODULE_WRAPPER&&) = delete;
	~HMODULE_WRAPPER()
	{
		if (m_handle)
		{
			dlclose(m_handle);
		}
	}

	HMODULE_WRAPPER& operator=(const HMODULE_WRAPPER&) = delete;
	HMODULE_WRAPPER& operator=(HMODULE_WRAPPER&&) = delete;

	operator bool() const { return m_handle != nullptr; }
	operator void*() const { return m_handle; }
private:
	void* m_handle;
};

const std::string& get_current_exe_path()
{
    static const std::string c_path = []() -> std::string {
		char buffer[255];
		int size_bytes = readlink("/proc/self/exe", buffer, 255);
		if (size_bytes == -1)
		{
			int err = errno;
			std::cout << "Error: Failed to get exe path. Error Code: " << strerror(err) << '\n';
			return std::string();
		}
		
		return buffer;
	}();
	
	return c_path;
}

//Important Note: For some unknown reason, the Unix version of the API requires a colon separated list, where the windows version requires a semi-colon separated one. This may be a bug
//CLR expects a /!\colon/!\ separated list of binaries to give full platform trust to
//Given that our LUA system is completely un-sandboxed (ask Mitch about his exe in a pack file), I don't think this is a big concern
std::string collect_trusted_binaries()
{
	namespace fs = std::filesystem;
	
	//Collect every dll in the same folder as game executable
	std::string result;
	for (fs::directory_iterator it = fs::directory_iterator(fs::current_path()); it != fs::directory_iterator(); ++it)
	{
		if (it->is_regular_file() && it->path().extension().compare(".dll") == 0)
		{
			result += (it->path().string() + ":");
		}
	}

	return result;
}

int get_cli_option()
{
	std::cout <<
		"******************\n"
		"* 0 - Exit       *\n"
		"* 1 - End Turn   *\n"
		"* 2 - Mem Usage  *\n"
		"******************\n";

	int option;
	std::cin >> option;
	return option;
}

void run_cli_interface(CAMPAIGN::CAMPAIGN_MODEL& model)
{

	bool run = true;

	while (run)
	{
		switch (get_cli_option())
		{
		case 0:
		{
			run = false;
			break;
		}
		case 1:
		{
			model.end_turn();
			break;
		}
		case 2:
		{
			std::cout << "\nHaven't been able to find a way to track memory usage from the host in Unix yet. Can always track it from the managed side though\n";
			break;
		}
		default:
			std::cout << "\nUnknown Option\n";
			break;
		}
	}
	

}

int main(int /*argc*/, const char* /*argv*/[])
{
	namespace fs = std::filesystem;
	
	// Load the CoreCLR Library ///////////////
	HMODULE_WRAPPER coreclr_lib(dlopen("DotNETCore/libcoreclr.so", RTLD_LAZY));
	if (!coreclr_lib)
	{
		const int err = errno;
		std::cout << "Error: Failed to load CoreCLR library. Error Code: " << strerror(err) << '\n';
		return -1;
	}

	coreclr_initialize_ptr const fn_coreclr_init = reinterpret_cast<coreclr_initialize_ptr>(dlsym(coreclr_lib, "coreclr_initialize"));
	if (fn_coreclr_init == nullptr)
	{
		const int err = errno;
		std::cout << "Error: Failed to retreive coreclr_initialize method from library. Error Code: " << strerror(err) << '\n';
		return -1;
	}
	
	coreclr_shutdown_ptr const fn_coreclr_shutdown = reinterpret_cast<coreclr_shutdown_ptr>(dlsym(coreclr_lib, "coreclr_shutdown"));
	if (fn_coreclr_shutdown == nullptr)
	{
		const int err = errno;
		std::cout << "Error: Failed to retreive coreclr_shutdown method from library. Error Code: " << strerror(err) << '\n';
		return -1;
	}
	
	coreclr_create_delegate_ptr fn_coreclr_create_delegate = reinterpret_cast<coreclr_create_delegate_ptr>(dlsym(coreclr_lib, "coreclr_create_delegate"));
	if (fn_coreclr_create_delegate == nullptr)
	{
		const int err = errno;
		std::cout << "Error: Failed to retreive coreclr_create_delegate method from library. Error Code: " << strerror(err) << '\n';
		return -1;
	}

	//The Unix API wraps a few of the Windows steps into one larger method
	std::array<const char*, 4> domain_property_keys {
		"TRUSTED_PLATFORM_ASSEMBLIES",
		"APP_PATHS",
		"NATIVE_DLL_SEARCH_DIRECTORIES",
		"APP_NI_PATHS"
	};

	//Colon separated on Unix for some reason
	const std::string trusted_binaries = collect_trusted_binaries();
	const std::string app_paths = fs::current_path().append("DotNETCore").string() + ":" + fs::current_path().append("Scripts").string() + ":";
	const std::string native_search_directories = fs::current_path().string() + ":" + fs::current_path().append("DotNETCore").string() + ":";

	std::array<const char*, 4> domain_property_values {
		trusted_binaries.c_str(),
		app_paths.c_str(),
		native_search_directories.c_str(),
		native_search_directories.c_str()
	};

	void* clr_host_handle = nullptr;
	unsigned int clr_domain_id = 0;
	fn_coreclr_init(
		get_current_exe_path().c_str(),
		"C# Root Domain",
		static_cast<int>(domain_property_keys.size()),
		domain_property_keys.data(),
		domain_property_values.data(),
		&clr_host_handle,
		&clr_domain_id
	);

	if (clr_host_handle == nullptr)
	{
		const int err = errno;
		std::cout << "Error: Failed to initialise CLR host. Error Code: " << strerror(err) << '\n';
		return -1;
	}

	//Create a callback into the managed library to bootstrap the script system
	using SCRIPT_BOOTSTRAP_FUNC = void(*)(CAMPAIGN::CAMPAIGN_MODEL& model);
	SCRIPT_BOOTSTRAP_FUNC scripts_bootstrap = nullptr;
	fn_coreclr_create_delegate(
		clr_host_handle,
		clr_domain_id,
		"GameScript.Core",
		"GameScript.Core.Bootstrap",
		"bootstrap",
		reinterpret_cast<void**>(&scripts_bootstrap)
	);

	if (scripts_bootstrap == nullptr)
	{
		const int err = errno;
		std::cout << "Error: Failed to get C# bootstrap method. Error Code: " << strerror(err) << '\n';
		return -1;
	}
	
	CAMPAIGN::CAMPAIGN_MODEL model;

	//Enter managed code
	scripts_bootstrap(model);

	run_cli_interface(model);

	// Shutdown CLR Host //////////////////////
	fn_coreclr_shutdown(clr_host_handle, clr_domain_id);
	
	return 0;
}

#endif //!WIN32

