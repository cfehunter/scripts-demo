#ifdef _WIN32

#include <array>
#include <chrono>
#include <filesystem>
#include <iostream>
#include <string>

#include <Windows.h>

extern "C"
{
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

#include "CampaignModel.h"
#include "CampaignModelLuaInterface.h"

//I'm using the model here, but you could use a script env or something else
using BOOTSTRAP_FUNC = bool (*)(CAMPAIGN::CAMPAIGN_MODEL*);
using UNLOADLIB_FUNC = void (*)();
using MEMSTATS_FUNC = void (*)();
using NAME_FUNC = const char* (*)();
using BENCHMARK_FUNC = void(*)(int);
struct ScriptBackend
{
	const char* m_name = "<UNKNOWN>";
	HMODULE        m_lib_handle = 0;
	UNLOADLIB_FUNC m_unload_lib_func = nullptr;

	//These functions are only required for the purposes of my demo
	BENCHMARK_FUNC m_benchmark_func = nullptr;
	BENCHMARK_FUNC m_fib_benchmark_func = nullptr;
	MEMSTATS_FUNC  m_mem_stats_func = nullptr;
};

using ScriptBackendVec = std::vector<ScriptBackend>;

ScriptBackendVec load_script_backend_plugins(CAMPAIGN::CAMPAIGN_MODEL& model)
{
	namespace fs = std::filesystem;
	const fs::path plugins_dir(fs::current_path().wstring() + L"/Plugins");

	ScriptBackendVec loaded_backends;
	if (fs::is_directory(plugins_dir))
	{
		const std::string c_dll_ext = ".dll";

		for (fs::directory_entry file : fs::directory_iterator(plugins_dir))
		{
			if (file.path().extension().string() == c_dll_ext)
			{
				HMODULE lib_handle = LoadLibraryW(file.path().c_str());
				if (lib_handle)
				{
					BOOTSTRAP_FUNC bootstrap_func = reinterpret_cast<BOOTSTRAP_FUNC>(GetProcAddress(lib_handle, "bootstrap"));
					NAME_FUNC name_func = reinterpret_cast<NAME_FUNC>(GetProcAddress(lib_handle, "name"));
					UNLOADLIB_FUNC unloadlib_func = reinterpret_cast<UNLOADLIB_FUNC>(GetProcAddress(lib_handle, "unload_lib"));
					BENCHMARK_FUNC benchmark_func = reinterpret_cast<BENCHMARK_FUNC>(GetProcAddress(lib_handle, "run_benchmark"));
					BENCHMARK_FUNC fib_benchmark_func = reinterpret_cast<BENCHMARK_FUNC>(GetProcAddress(lib_handle, "run_benchmark_fib"));
					MEMSTATS_FUNC mem_stats_func = reinterpret_cast<MEMSTATS_FUNC>(GetProcAddress(lib_handle, "print_mem_stats"));

					if (bootstrap_func && unloadlib_func && benchmark_func && fib_benchmark_func && bootstrap_func(&model))
					{
						ScriptBackend& new_backend = loaded_backends.emplace_back();
						new_backend.m_name = name_func();
						new_backend.m_lib_handle = lib_handle;
						new_backend.m_unload_lib_func = unloadlib_func;
						new_backend.m_benchmark_func = benchmark_func;
						new_backend.m_fib_benchmark_func = fib_benchmark_func;
						new_backend.m_mem_stats_func = mem_stats_func;

						std::cout << "Loaded Script Backend: " << new_backend.m_name << '\n';
					}
					else
					{
						FreeLibrary(lib_handle);
					}
				}
			}
		}
	}

	return loaded_backends;
}

void split_nano_seconds(size_t& nano_seconds, size_t& seconds, size_t& milliseconds, size_t& microseconds)
{
	seconds = nano_seconds / (1000 * 1000 * 1000);
	nano_seconds -= seconds * (1000 * 1000 * 1000);

	milliseconds = nano_seconds / (1000 * 1000);
	nano_seconds -= milliseconds * (1000 * 1000);

	microseconds = nano_seconds / 1000;
	nano_seconds -= microseconds * 1000;
}

void run_lua_benchmark(CAMPAIGN::CAMPAIGN_MODEL& model)
{
	size_t seconds;
	size_t milliseconds;
	size_t microseconds;
	size_t nano_seconds;

	lua_State* ls = luaL_newstate();
	luaL_openlibs(ls);
	Lunar<CAMPAIGN_LUA_INTERFACE>::Register(ls);
	CAMPAIGN_LUA_INTERFACE::create_and_push_to_stack(ls, model);
	lua_setglobal(ls, "model");

	std::cout << "Running LUA Benchmark. Generating 10,000,000 random numbers across the interface boundary\n";
	using timer = std::chrono::high_resolution_clock;
	timer::time_point start = timer::now();

	//Note: I am being *very* generous to lua here by not creating a new rng script interface per-call
	luaL_dofile(ls, "BenchmarkRng.lua");

	timer::time_point end = timer::now();
	nano_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
	split_nano_seconds(nano_seconds, seconds, milliseconds, microseconds);
	std::cout << "Benchmark Time:\n" << seconds << " s " << milliseconds << " ms " << microseconds << " us " << nano_seconds << " ns\n";

	std::cout << "Running LUA Benchmark. Generating 1000th Fibonnaci number\n";
	start = timer::now();

	luaL_dofile(ls, "BenchmarkFib.lua");
	end = timer::now();
	nano_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
	split_nano_seconds(nano_seconds, seconds, milliseconds, microseconds);
	std::cout << "Benchmark Time:\n" << seconds << " s " << milliseconds << " ms " << microseconds << " us " << nano_seconds << " ns\n";
}

void run_benchmark(BENCHMARK_FUNC rng_fnc, BENCHMARK_FUNC fib_fnc, CAMPAIGN::CAMPAIGN_MODEL& model)
{
	size_t seconds;
	size_t milliseconds;
	size_t microseconds;
	size_t nano_seconds;

	std::cout << "Running Worst Case C# Benchmark. Generating 10,000,000 random numbers across the managed/unmanaged boundary\n";

	using timer = std::chrono::high_resolution_clock;
	timer::time_point start = timer::now();

	rng_fnc(10000000);

	timer::time_point end = timer::now();
	nano_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
	split_nano_seconds(nano_seconds, seconds, milliseconds, microseconds);

	std::cout << "Benchmark Time:\n" << seconds << " s " << milliseconds << " ms " << microseconds  << " us " << nano_seconds << " ns\n";

	std::cout << "Running C# Benchmark. Generating 1000th Fibonnaci number\n";
	start = timer::now();

	fib_fnc(1000);
	end = timer::now();
	nano_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
	split_nano_seconds(nano_seconds, seconds, milliseconds, microseconds);
	std::cout << "Benchmark Time:\n" << seconds << " s " << milliseconds << " ms " << microseconds << " us " << nano_seconds << " ns\n";

	run_lua_benchmark(model);
}

int get_cli_option()
{
	std::cout <<
		"******************\n"
		"* 0 - Exit       *\n"
		"* 1 - End Turn   *\n"
		"* 2 - Mem Usage  *\n"
		"* 3 - Benchmark  *\n"
		"******************\n";

	int option;
	std::cin >> option;
	return option;
}

void run_cli_interface(CAMPAIGN::CAMPAIGN_MODEL& model, const ScriptBackend* const backend)
{

	bool run = true;

	while (run)
	{
		switch (get_cli_option())
		{
		case 0:
		{
			run = false;
			break;
		}
		case 1:
		{
			model.end_turn();
			break;
		}
		case 2:
		{
			if (backend)
			{
				backend->m_mem_stats_func();
			}
			else
			{
				std::cout << "\nError: No script backends loaded\n";
			}
			break;
		}
		case 3:
		{
			if (backend)
			{
				run_benchmark(backend->m_benchmark_func, backend->m_fib_benchmark_func, model);
			}
			else
			{
				std::cout << "\nError: No script backends loaded\n";
			}
			break;
		}
		default:
			std::cout << "\nUnknown Option\n";
			break;
		}
	}
	

}

int main(int /*argc*/, const char* /*argv*/[])
{
	CAMPAIGN::CAMPAIGN_MODEL model;
	MessageBoxA(nullptr, "Holding for Debugger", "Wait", MB_OK);
	
	//Load the backends
	ScriptBackendVec backends = load_script_backend_plugins(model);

	run_cli_interface(model, backends.empty() ? nullptr : &backends.front());

	// Shutdown script backends
	for (const ScriptBackend& backend : backends)
	{
		//You could do this during runtime to hotload the backend completely
		backend.m_unload_lib_func();
		FreeLibrary(backend.m_lib_handle);
	}

	return 0;
}

#endif
