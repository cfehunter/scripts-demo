#include "CampaignModelLuaInterface.h"

#include "CampaignModel.h"

extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

CAMPAIGN_LUA_INTERFACE::CAMPAIGN_LUA_INTERFACE(lua_State* /*ls*/)
	: m_model(nullptr)
{ }

CAMPAIGN_LUA_INTERFACE::CAMPAIGN_LUA_INTERFACE(CAMPAIGN::CAMPAIGN_MODEL& model)
	: m_model(&model)
{ }

CAMPAIGN_LUA_INTERFACE::~CAMPAIGN_LUA_INTERFACE()
{ }

int CAMPAIGN_LUA_INTERFACE::random_number(lua_State* ls)
{
	int min = static_cast<int>(luaL_checknumber(ls, 1));
	int max = static_cast<int>(luaL_checknumber(ls, 2));

	lua_pushnumber(ls, m_model->random_number(min, max));
	return 1;
}

int CAMPAIGN_LUA_INTERFACE::create_and_push_to_stack(lua_State* ls, CAMPAIGN::CAMPAIGN_MODEL& model)
{
	Lunar<CAMPAIGN_LUA_INTERFACE>::push(ls, new CAMPAIGN_LUA_INTERFACE(model), true);
	return 1;
}

const char CAMPAIGN_LUA_INTERFACE::className[] = "Campaign";
Lunar<CAMPAIGN_LUA_INTERFACE>::RegType CAMPAIGN_LUA_INTERFACE::methods[] = {
	LUNAR_DECLARE_METHOD(CAMPAIGN_LUA_INTERFACE, random_number),
	{0, 0}
};
